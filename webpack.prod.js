const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const webpack = require("webpack");
module.exports = {
  mode: "production",
  entry: "./src/app/index.js",
  output: {
    filename: "main.[contentHash].js",
    path: path.resolve(__dirname, "build")
  },
  optimization: {
    minimizer: [new TerserPlugin(), new OptimizeCssAssetsPlugin()]
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new HTMLWebpackPlugin({
      template: "./src/template.html"
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "css/[name].[contentHash].css"
    })
  ],
  module: {
    rules: [
      {
        test: /\.js&/,
        use: ["babel-loader"],
        exclude: "/node_modules"
      },
      {
        test: /\.scss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              config: { path: "src/app/js/postcss.config.js" }
            }
          },
          "sass-loader"
        ],
        exclude: "/node_modules"
      },
      {
        test: /\.css$/i,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          {
            loader: "postcss-loader",
            options: {
              config: { path: "src/app/js/postcss.config.js" }
            }
          }
        ],
        exclude: "/node_modules"
      },
      {
        test: /\.html$/,
        use: ["html-loader"]
      },
      {
        test: /\.(ttf|eot|svg|png|jpg|gif|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            outputPath: "media",
            esModule: false
          }
        }
      }
    ]
  }
};
